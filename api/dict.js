import request from '@/utils/request'

const api_name = `/admin/cmn/dict`
export default {
    //根据dictCode获取下级节点
    findByDictCode(dictcode) {
        return request ({
            url: `${api_name}/findDictCode/${dictcode}`,
            method: 'get',
        })
    },

    //根据数据id查询子数据列表
    findByParentId(parentId) {
        return request ({
            url: `${api_name}/findChildData/${parentId}`,
            method: 'get',
        })
    },
}